import random

def mmz():
    c1 = []
    c1k = input("输入密码字").lower()
    for i in c1k:#保证不重复
        if ord(i)-ord('a') not in c1 and ord(i)-ord('a') <= 15:
            c1 += [ord(i)-ord('a')]
    c1 += [i for i in range(16) if i not in c1]
    print(f"密钥1为{c1}")
    global c
    c += [c1]

def xpf():
    c2 = [i for i in range(16)]
    for i in range(15,0,-1):#与下面的一张牌交换
        rand = random.randint(0,i-1)
        (c2[i],c2[rand]) = (c2[rand],c2[i])
    print(f"密钥2为{c2}")
    global c
    c += [c2]

def gsf():
    c3 = [(7*i+3)%16 for i in range(16)]#用公式生成
    print(f"密钥3为{c3}")
    global c
    c += [c3]

def ff1():
    c4 = [i for i in range(16)]
    ck4  = [random.randint(0,15) for _ in range(16)]
    for i in range(15,1,-1):
        c4[i],c4[(ck4[i]+ck4[i-1])%16] = c4[(ck4[i]+ck4[i-1])%16],c4[i]
    print(f"密钥4为{c4}")
    global c
    c += [c4]

def ff2():
    ck5 = [random.randint(0,30) for _ in range(30)]
    c5=[]
    for i in range(len(ck5)):
        if ck5[i]%16 not in c5:
            c5 += [ck5[i]%16]
    for i in range(16):
        if i not in c5:
            c5 += [i]
    print(f"密钥5为{c5}")
    global c
    c += [c5]

if __name__ == '__main__':
    c = []
    mmz()
    xpf()
    gsf()
    ff1()
    ff2()
    print("移位表如下:")
    print('  ',end='')
    for i in range(16):
        print(i,end=' ')
    print('')
    for i in range(5):
        print(i,end=' ')
        for j in range(16):
            print(c[i][j],end=' ')
        print('')
    try:
        f = open("test.txt",'r')
    except FileNotFoundError:
        print('文件读取错误')
        exit()
    str = f.read()
    print("明文为:")
    print(str)
    while(len(str)%16 != 0):
        str+='z'
    str_new = [0]*len(str)
    m = []
    for i in range(len(str)//16):
        for j in range(16):
            str_new[16*i+j] = str[16*i+c[i%5][j]]
    print("密文为:")
    print(''.join(str_new))
